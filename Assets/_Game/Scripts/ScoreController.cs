﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScoreController : MonoBehaviour {

	public static int Score { get; set; }
	public AudioSource captureSound;

	public Text scoreText;

	void OnTriggerEnter2D(Collider2D col) {
		if(col.gameObject.CompareTag("Person") && UFO.health > 0) {
			DestroyObject(col.gameObject);
			captureSound.Play();
			Score++;
			scoreText.text = "Score: " + Score;
		}
	}
}
