﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Menu : MonoBehaviour {

	public GameObject[] uiElements;
	public Text scoreText;

	void Start() {
		GameController.Instance().menu = this;
	}

	public void StartGame() {
		ScoreController.Score = 0;
		scoreText.text = "Score: 0";
		UFO.health = 3;
		UFO.UfoTransform.position = new Vector3(UFO.UfoTransform.position.x, 3.5f, UFO.UfoTransform.position.z);
		for(int i = 0; i < uiElements.GetLength(0); i++) {
			uiElements[i].SetActive(false);
		}
	}

	public void Dead() {
		for(int i = 0; i < uiElements.GetLength(0); i++) {
			uiElements[i].SetActive(true);
		}
	}

}
