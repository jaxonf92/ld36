﻿using UnityEngine;
using System.Collections;

public class Person : MonoBehaviour {

	public float speed;
	public float killSpeed;
	public GameObject deadPerson;

	private Rigidbody2D rb;
	private bool grounded;
	private float floatSpeed;
	public bool InBean { get; set; }

	void Start() {
		rb = GetComponent<Rigidbody2D>();
		floatSpeed = Random.Range(1.5f, 2.5f);
	}

	void Update() {
		InBean = UFO.TractorBeanOn ? InBean : false;
		if(InBean) {
			grounded = false;
			Vector2 direction = ((Vector2) (UFO.UfoTransform.position - transform.position)).normalized;
			rb.velocity = direction * floatSpeed;
		} else if(grounded && transform.eulerAngles.z == 0f) {
			rb.velocity = Vector2.right * speed;
		}
		if(transform.position.x < (Spawner.LeftBoundary.position.x - 1) ||
			transform.position.x > (Spawner.RightBoundary.position.x + 1)) {
			Destroy(gameObject);
		}
	}

	void OnCollisionEnter2D(Collision2D col) {
		grounded = true;
		if(rb.velocity.magnitude > killSpeed) {
			GameObject newDeadPerson = Instantiate(deadPerson);
			newDeadPerson.transform.position = transform.position;
			Destroy(newDeadPerson, 30);
			Destroy(gameObject);
		}
	}
}
