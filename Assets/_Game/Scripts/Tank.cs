﻿using UnityEngine;
using System.Collections;

public class Tank : MonoBehaviour {

	public static int MaxTanks { get; set; }
	public static int CurrentTanks { get; set; }
	public float speed;
	public float killSpeed;
	public GameObject deadTank;
	public float rotationSmoothTime;
	public GameObject projectile;
	public Transform projectileExit;
	public Transform turrent;
	public AudioSource shootSound;

	private Rigidbody2D rb;
	private bool grounded;
	private float floatSpeed;
	private float shootingAngle;
	private bool isShooting;
	private float rotationVelocity;
	private float lastShootTime;
	public bool InBean { get; set; }

	void Start() {
		rb = GetComponent<Rigidbody2D>();
		floatSpeed = 0.8f;
		isShooting = false;
	}

	void Update() {
		InBean = UFO.TractorBeanOn ? InBean : false;
		if(InBean) {
			grounded = false;
			Vector2 direction = ((Vector2) (UFO.UfoTransform.position - transform.position)).normalized;
			rb.velocity = direction * floatSpeed;
		} else if(grounded && transform.eulerAngles.z == 0f) {
			rb.velocity = Vector2.right * speed;
		}
		if(transform.position.x < (Spawner.LeftBoundary.position.x - 1) ||
			transform.position.x > (Spawner.RightBoundary.position.x + 1)) {
			Tank.CurrentTanks--;
			Destroy(gameObject);
		}
		Shoot();
	}

	private void Shoot() {
		if(!isShooting) {
			isShooting = true;
			Vector3 toShip = UFO.UfoTransform.position - transform.position;
			Vector2 toShip2D = new Vector2(toShip.x, toShip.y);
			shootingAngle = Vector2.Angle(Vector2.up, toShip2D);
			shootingAngle *= UFO.UfoTransform.position.x > transform.position.x ? -1 : 1;
			shootingAngle += Random.Range(-5, 5);
			Debug.Log("Angle: " + shootingAngle);
		}
		float newAngle = Mathf.SmoothDampAngle(turrent.eulerAngles.z, shootingAngle, ref rotationVelocity,  rotationSmoothTime);
		turrent.eulerAngles = Vector3.forward * newAngle;
		if(isShooting && Mathf.DeltaAngle(turrent.eulerAngles.z, shootingAngle)< 1 && Time.time - lastShootTime > 1 && UFO.health > 0) {
			shootSound.Play();
			lastShootTime = Time.time;
			GameObject newProjectile = Instantiate(projectile);
			Destroy(newProjectile, 6);
			newProjectile.transform.position = projectileExit.position;
			newProjectile.GetComponent<Projectile>().Direction = Quaternion.AngleAxis(shootingAngle + 90, Vector3.forward) * Vector3.right;
			newProjectile.GetComponent<Projectile>().UpdateVelocity();
			Debug.Log("Direction: x: " + newProjectile.GetComponent<Projectile>().Direction.x + ", y: " + newProjectile.GetComponent<Projectile>().Direction.y);
			isShooting = false;
		}
	}

	void OnCollisionEnter2D(Collision2D col) {
		grounded = true;
		if(rb.velocity.magnitude > killSpeed) {
			GameObject newDeadPerson = Instantiate(deadTank);
			newDeadPerson.transform.position = transform.position;
			Destroy(newDeadPerson, 30);
			Destroy(gameObject);
		}
	}

}
