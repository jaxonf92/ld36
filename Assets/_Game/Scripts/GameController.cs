﻿using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour {

	private static GameController singleton;

	public float cloudSpeed;
	public float cloudSize;
	public Menu menu;

	public void Awake() {
		if(singleton == null) {
			singleton = this;
		} 
	}

	void Start() {
		Tank.MaxTanks = 3;
		Tank.CurrentTanks = 1;
	}

	public static GameController Instance() {
		return singleton;
	}

}
