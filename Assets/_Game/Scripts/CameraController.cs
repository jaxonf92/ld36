﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {

	public Transform target;
	public float smoothTime;

	private float velocity;
	private float position;

	void Start() {
		position = transform.position.x;
	}

	void Update () {
		position = Mathf.SmoothDamp(position, target.position.x, ref velocity, smoothTime);
		transform.position = new Vector3(position, transform.position.y, transform.position.z);
	}
}
