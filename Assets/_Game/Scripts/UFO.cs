﻿using UnityEngine;
using System.Collections;

public class UFO : MonoBehaviour {

	public float speed;
	public float smoothTime;
	public float moveAngle;
	public float rotationSmoothTime;
	public float movingRotationModifier;
	public GameObject tractorBean;
	public AudioSource tractorBeamOffSound;
	public AudioSource hitSound;
	public AudioSource deadSound;
	public ParticleSystem ps;
	public Color lightSmoke;
	public Color heavySmoke;
	private Vector3 velocity;
	private float rotationVelocity;
	public static int health;

	public static Transform UfoTransform { get; private set; }
	public static bool TractorBeanOn { get; private set; }

	void Start() {
		UfoTransform = transform;
		health = 0;
	}

	void Update () {
		if(health == 3) {
			Reset();
		}
		if(health > 0) {
			UpdatePosition();
			UpdateRotation();
			TractorBeanUpdate();
		} else if(transform.position.y > -4.5){
			Vector3 newPosition = Vector3.SmoothDamp(transform.position, new Vector3(transform.position.x, -8f, transform.position.z) , ref velocity, smoothTime * 0.6f);
			transform.position = newPosition;
		}
	}

	private void UpdatePosition() {
		Vector3 newPosition = Vector3.SmoothDamp(transform.position, transform.position + Vector3.right * Input.GetAxisRaw("Horizontal") * speed , ref velocity, smoothTime);
		transform.position = newPosition;
	}

	private void UpdateRotation() {
		float targetAngle = 0;
		if(Input.GetAxisRaw("Horizontal") > 0) {
			targetAngle = moveAngle * -1;
		} else if(Input.GetAxisRaw("Horizontal") < 0) {
			targetAngle = moveAngle;
		}
		float newAngle = Mathf.SmoothDampAngle(transform.eulerAngles.z, targetAngle, ref rotationVelocity,  targetAngle == 0f ? rotationSmoothTime : rotationSmoothTime * movingRotationModifier);
		transform.eulerAngles = Vector3.forward * newAngle;
	}

	private void TractorBeanUpdate() {
		if(TractorBeanOn && !Input.GetButton("Jump")) {
			tractorBeamOffSound.Play();
		}
		TractorBeanOn = Input.GetButton("Jump");
		tractorBean.SetActive(TractorBeanOn);

	}

	private void Reset() {
		ps.emissionRate = 0;
		ps.startColor = lightSmoke;
	}

	private void Damage() {
		health--;
		ps.emissionRate = health == 2 ? 2 : 10;
		ps.startColor = health == 2 ? lightSmoke : heavySmoke;
		if(health == 0) {
			Death();
		} else if (health > 0) {
			hitSound.Play();
		}
	}
	private void Death() {
		deadSound.Play();
		GameController.Instance().menu.Dead();
	}

	void OnTriggerEnter2D(Collider2D col) {
		if(col.gameObject.CompareTag("Person")) {
			col.gameObject.GetComponent<Person>().InBean = true;
		} else if(col.gameObject.CompareTag("Projectile")) {
			Destroy(col.gameObject);
			Damage();
		}
	}

	void OnTriggerExit2D(Collider2D col) {
		if(col.gameObject.CompareTag("Person")) {
			col.gameObject.GetComponent<Person>().InBean = false;
			//col.gameObject.SendMessage("Suction");
		}
	}
}
