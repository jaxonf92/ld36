﻿using UnityEngine;
using System.Collections;

public class Cloud : MonoBehaviour {

	private float cloudSpeed;

	public float modifyer = 1;

	public void Start() {
		float cloudSize = GameController.Instance().cloudSize;
		transform.localScale = new Vector3(cloudSize * modifyer, cloudSize * modifyer, 1);
		cloudSpeed = GameController.Instance().cloudSpeed * modifyer;
	}

	public void Update() {
		transform.position = Vector3.MoveTowards(transform.position, transform.position + Vector3.right, cloudSpeed * Time.deltaTime);
	}

}
