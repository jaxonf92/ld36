﻿using UnityEngine;
using System.Collections;

public class Spawner : MonoBehaviour {

	public float spawnerTime;
	public float minSpeed;
	public float maxSpeed;
	public GameObject person;
	public GameObject tank;
	public bool isLeft;

	public static Transform LeftBoundary { get; private set; }
	public static Transform RightBoundary { get; private set; }

	private float lastSpawn;

	void Start() {
		if(isLeft) {
			LeftBoundary = transform;
		} else {
			RightBoundary = transform;
		}
	}

	void Update() {
		if(Time.time - lastSpawn > spawnerTime) {
			lastSpawn = Time.time;
			if(Tank.CurrentTanks < Tank.MaxTanks && Random.Range(0, 10) == 0) {
				Tank.CurrentTanks++;
				GameObject newTank = Instantiate(tank);
				newTank.transform.position = transform.position;
				newTank.GetComponent<Tank>().speed *= isLeft ? 1 : -1;
			}
			GameObject newPerson = Instantiate(person);
			newPerson.transform.position = transform.position;
			newPerson.GetComponent<Person>().speed = Random.Range(minSpeed, maxSpeed);

		}
	}

}
