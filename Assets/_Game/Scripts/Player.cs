﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

	public float speed;
	public float smoothTime;

	private Vector3 velocity;
	private Rigidbody2D rb;

	public void Start() {
		rb = GetComponent<Rigidbody2D>();
	}

	public void Update() {
		velocity = (new Vector3(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"), 0f).normalized * speed);
		transform.position = Vector3.SmoothDamp(transform.position, transform.position + velocity, ref velocity, smoothTime);
	}

	
}
