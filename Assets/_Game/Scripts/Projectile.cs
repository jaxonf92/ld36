﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour {

	public float speed;
	public Vector2 Direction { get; set; }
	private Rigidbody2D rb;

	void Start() {
		rb = GetComponent<Rigidbody2D>();
		rb.velocity = Direction * speed;
	}

	void Update() {
	}

	public void UpdateVelocity() {
		
	}

}
