﻿using UnityEngine;
using System.Collections;

public class TractorBeam : MonoBehaviour {

	void OnTriggerEnter2D(Collider2D col) {
		if(col.gameObject.CompareTag("Person")) {
			col.gameObject.GetComponent<Person>().InBean = true;
			//col.gameObject.SendMessage("Suction");
		}
	}

	void OnTriggerExit2D(Collider2D col) {
		if(col.gameObject.CompareTag("Person")) {
			col.gameObject.GetComponent<Person>().InBean = false;
			//col.gameObject.SendMessage("Suction");
		}
	}
}
